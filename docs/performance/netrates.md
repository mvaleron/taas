Opencast performance
===

[TOC]

# Sources

Test file: 
| From             | Zone          | To         | Zone          | Storage | Path |
| ---------------- | ------------- | ---------- | ------------- | ------- | ---- |
| ocworker-prod-08 | cern-geneva-c | ocweb-test | cern-geneva-c | Local   |  /storage/opencast    |

 curl --progress-bar --remote-name --location https://videos.cern.ch/api/files/44fa70f3-ca7a-4af4-843d-f1661d16e2c8/CERN-FOOTAGE-2020-055-001.mov?versionId=89da7b1a-965e-411c-ab2e-74d296276bbc&download -o CERN-FOOTAGE-2020-055-001.mov

Downloading: 
| File system | Incoming rates (Mbps) |
| ----------- | --------------------- |
| local       | 1228.81 Mbps          |
| fuse        | 424.35 Mbps           |
| kernel      | 1177.21 Mbps          |

```bash
/storage/opencast/files/mediapackage/4359e695-16fe-4e82-abcb-1503d4a2492e/0c31f69f-d582-4c61-aeb8-e4a336d06d91/total 64G
-rw-r--r--. 1 opencast opencast 64G Jan 19 11:51 camera.mov
-rw-r--r--. 1 opencast opencast  32 Jan 19 11:51 camera.mov.md5
```

| Duration | 11:36   |
| -------- | ------- |
| Size     | 68.2 GB |

| Type                      | Frame rate | Bit rate  | Resolution | Frame count |
| ------------------------- | ---------- | --------- | ---------- | ----------- |
| Apple ProRes (iCodec Pro) | 25         | 782865090 | 3840x2160  | 17418       |

# Ingest end encoding operations

Test Local
====

| Operation    | Started       | Finished    | Total  |
| ------------ | ------------- | ----------- | ------ |
| Test Env     | Local storage |             |        |
| ----------   | --------      | ----------  | -----: |
| Created      | 11:34:00 AM   |             |        |
| WF Starts    | 11:51:07 AM   |             | 0h17m  |
| Inspect      | 11:51:23 AM   | 12:02:00 PM | 11m    |
| Encode 720p  | 12:02:01 PM   | 12:26:55 PM | 24m    |
| Encode 1080p | 12:27:03 PM   | 12:59:22 PM | 32m    |
| Encode 480p  | 12:59:35 PM   | 1:21:52 PM  | 22m    |
| Encode 360p  | 1:22:06 PM    | 1:44:19 PM  | 22m    |
| Encode 2160p | 1:44:33 PM    | 2:43:19 PM  | 1h01m  |
|              |               |             | 2h55m  |

Test CEPHs fuse
====

| Operation    | Started     | Finished    | Total |
| ------------ | ----------- | ----------- | -----:|
| Created      | 7:06:00 PM  |             |       |
| WF Starts    | 8:53:13 PM  |             | 1h47m |
| Inspect      | 8:53:21 PM  | 9:37:51 PM  |   44m |
| Encode 720p  | 9:37:53 PM  | 10:04:27 PM |   27m |
| Encode 1080p | 10:38:45 PM | 10:38:55 PM |    -- |
| Encode 480p  | 10:38:57 PM | 11:03:25 PM |   25m |
| Encode 360p  | 11:03:39 PM | 11:27:36 PM |   24m |
| Encode 2160p | 11:27:51 PM | 12:28:33 AM |  1h1m |
|              |             |             |       |


| Operation    | Started     | Finished   |   Total |
|:------------ | ----------- | ---------- | -------:|
| Created      | 00:53:00 PM |            |         |
| WF Starts    | 2:42:18 AM  |            |   1h49m |
| Inspect      | 2:42:27 AM  | 3:30:27 AM |     48m |
| Encode 720p  | 3:30:27 AM  | 3:57:16 AM |     27m |
| Encode 1080p | 3:57:25 AM  | 4:31:49 AM |     34m |
| Encode 480p  | 4:32:02 AM  | 4:56:55 AM |     24m |
| Encode 360p  | 4:57:08 AM  | 5:21:01 AM |     24m |
| Encode 2160p | 5:21:15 AM  | 6:21:56 AM |    1h0m |
| Exec. time   |             |            | 4:06:39 |
|              |             |            |         |

Prod CEPHs fuse (origin ocworker-prod-08 geneva-c -> ocweb-prod geneva-c /mnt/opencast_share)
====

| Operation    | Started     | Finished    |   Total |
| ------------ | ----------- | ----------- | -------:|
| Created      | 8:41:00 AM  |             |         |
| WF Starts    | 11:11:58 AM |             |   2h30m |
| Inspect      | 11:12:01 AM | 12:12:35 PM |      1h |
| Encode 720p  | 12:12:35 PM | 12:31:44 PM |     19m |
| Encode 1080p | 12:31:53 PM | 12:51:56 PM |     20m |
| Encode 480p  | 12:52:15 PM | 1:09:37 PM  |     17m |
| Encode 360p  | 1:09:52 PM  | 1:28:04 PM  |     19m |
| Encode 2160p | 1:28:19 PM  | 1:56:03 PM  |     28m |
| Exec. time   |             |             | 2:44:36 |
|              |             |             |         |

Prod CEPHs fuse (origin ocworker-prod-06 geneva-a -> ocweb-prod geneva-c /root)
====

| Operation    | Started    | Finished   | Total   |
| ------------ |:---------- | ---------- |:------- |
| created      | 3:47 PM    |            |         |
| WF Stats     | 6:12:16 PM |            | 2h25m   |
| Inspect      | 6:12:20 PM | 7:30:15 PM | 1h18m   |
| Encode 720p  | 7:30:16 PM | 7:49:29 PM | 19m     |
| Encode 1080p | 7:49:36 PM | 8:09:39 PM | 20m     |
| Encode 480p  | 8:09:52 PM | 8:27:19 PM | 18m     |
| Encode 360p  | 8:27:30 PM | 8:44:48 PM | 17m     |
| Encode 2160p | 8:45:00 PM | 9:11:04 PM | 26m     |
| Exec. time   |            |            | 2:59:14 |

Test CEPHs kernel (origin ocworker-prod-08 geneva-c -> ocweb-test geneva-c /mnt/opencast_share)
====

| Operation    | Started     | Finished    |   Total |
| ------------ | ----------- | ----------- | -------:|
| Created      | 9:29:00 AM  |             |         |
| WF Starts    | 9:47:06 AM  |             |     18m |
| Inspect      | 9:47:14 AM  | 10:07:58 AM |     20m |
| Encode 720p  | 10:07:59 AM | 10:33:47 AM |     25m |
| Encode 1080p | 10:33:55 AM | 11:06:44 AM |     32m |
| Encode 480p  | 11:06:58 AM | 11:29:43 AM |     22m |
| Encode 360p  | 11:29:56 AM | 11:52:18 AM |     23m |
| Encode 2160p | 11:52:31 AM | 12:53:23 AM |    1h1m |
| Exec. time   |             |             | 3:06:39 |
|              |             |             |         |


| Operation    | Started     | Finished    |   Total |
| ------------ | ----------- | ----------- | -------:|
| Created      | 9:59:00 AM  |             |         |
| WF Starts    | 10:16:23 AM |             |     17m |
| Inspect      | 10:16:28 AM | 10:28:35 AM |     12m |
| Encode 720p  | 10:28:36 AM | 10:54:45 AM |     26m |
| Encode 1080p | 10:54:53 AM | 11:28:33 AM |     33m |
| Encode 480p  | 11:28:46 AM | 11:51:58 AM |     23m |
| Encode 360p  | 11:52:12 AM | 12:14:35 AM |     22m |
| Encode 2160p | 12:14:48 PM | 01:14:25 PM |      1h |
| Exec. time   |             |             | 2:58:22 |
|              |             |             |         |
|              |             |             |         |

QA CEPHs kernel
====

| Operation    | Started     | Finished    | Total |
| ------------ | ----------- | ----------- | -----:|
| Created      | 10:27:28 AM |             |       |
| WF Starts    | 10:41:00 AM |             |   14m |
| Inspect      | 10:48:00 AM | 11:06:03 AM |   18m |
| Encode 720p  |             | 1           |       |
| Encode 1080p |             |             |       |
| Encode 480p  |             |             |       |
| Encode 360p  |             |             |       |
| Encode 2160p |             |             |       |
| Exec. time   |             |             |       |
|              |             |             |       |
|              |             |             |       |
|              |             |             |       |

Comparision table
====

| Operation    | Local | CEPH test (fuse) | CEPH prod (fuse) | CEPH test (kernel) |
| ------------ | -----:| ----------------:| ----------------:| ------------------:|
| Ingest       | 0h17m |            1h47m |            2h30m |                17m |
| Inspect      |   11m |              44m |               1h |                12m |
| Encode 360p  |   22m |              24m |              19m |                23m |
| Encode 480p  |   22m |              25m |              17m |                22m |
| Encode 720p  |   24m |              27m |              19m |                25m |
| Encode 1080p |   32m |              34m |              20m |                32m |
| Encode 2160p | 1h01m |             1h1m |              28m |               1h1m |
| Exec time    | ~2h55m |            4h06m |            2h44m |              2h58m |


Network rates (grafana)
====


ocweb-prod

![](https://codimd.web.cern.ch/uploads/upload_8a6231ea6908d62807f27e6d781a157a.png)

ocworker-prod-08

![](https://codimd.web.cern.ch/uploads/upload_7d3759a0b6fc09ff8d362ba40d0ef9b7.png)


Network rates (iptraf-ng)
====

| Environment  | Incoming (kbps) |
|:------------ | ---------------:|
| Test   Local |       542593.52 |
|              |       548628.86 |
| Test CEPHfs  |        97738.33 |
| fuse         |        97747.05 |

| Environment | Incoming  (kbps) |
|:----------- | ----------------:|
| Test CEPHfs |        547853.46 |
| kernel      |        548236.74 |
|             |        582711.51 |

| Environment  | Incoming (kbps) |
|:------------ | ---------------:|
| Prod  CEPHfs |        63879.06 |
| fuse         |        64133.34 |
|              |         3607.66 |

Network rates compared
====

| Role     | Total   (kbps) |   pps |  Incoming |  pps |  Outgoing |   pps |
|:-------- | --------------:| -----:| ---------:| ----:| ---------:| -----:|
| Receiver |      128678.93 |  1228 |  61599.22 |  763 |  67079.70 |   465 |
| fuse     |      183637.14 |  1769 |  90181.57 | 1126 |  93455.57 |   642 |
| Sender   |       65679.94 |  2075 |    368.54 |  815 |  65311.39 |  1259 |
| fuse     |       62635.54 |  2957 |   1536.32 | 1331 |  61099.22 |  1625 |
|          |       62371.82 |  1965 |    357.91 |  794 |  62013.90 |  1170 |
|          |                |       |           |      |           |       |
| Receiver |      550508.78 | 16039 | 547535.62 | 9067 |   2973.16 |  6917 |
| kernel   |                |       |           |      |           |       |
| Sender   |      562752.62 | 21487 |   3831.70 | 9448 | 558920.92 | 12039 |
| fuse     |                |       |           |      |           |       |

# System setup

From fuse to kernel
====

```puppet
  file {'/mnt/opencast_share':
    ensure => directory,
  }
  -> cephfs {'/mnt/opencast_share':
    cluster     => $ceph_opencast_cluster,
    remote_path => $ceph_opencast_remote,
    id          => $ceph_opencast_id,
    type        => 'kernel'
  }
  ```
  
/etc/fstab

```bash
none    /mnt/opencast_share     fuse.ceph       ceph.id=opencast_test_01_rw,ceph.conf=/etc/ceph/dwight.conf,ceph.client_mountpoint=/volumes/_nogroup/90e16532-f3e1-4c90-85a8-11e70572914e,x-systemd.device-timeout=30,x-systemd.mount-timeout=30,noatime,_netdev 
```

```
188.184.86.25:6790,188.184.94.56:6790,188.185.66.208:6790:/volumes/_nogroup/90e16532-f3e1-4c90-85a8-11e70572914e on /mnt/opencast_share type ceph (rw,noatime,seclabel,name=opencast_test_01_rw,secret=<hidden>,acl)
```


# Files transfer operation


File copy from local to CEPHfs fuse
====
ocworker-prod-8 Zone: geneva-c


rsync --info=progress2 camera.mov /mnt/opencast_share/camera.mov

  | Bytes          | Percentage | Speed      | Time    |
  | -------------- | ---------- | ---------- | ------- |
  | 1,507,098,624  | 2%         | 141.93MB/s | 0:07:38 |
  | 10,148,741,120 | 14%        | 161.11MB/s | 0:05:51 |
  | 60,772,089,856 | 89%        | 166.67MB/s | 0:00:43 |


| Environment  | Total (Mbps) |  pps | Incoming (kbps) |  pps | Outgoing (Mbps) | pps  |
|:------------ | ------------:| ----:| --------------- | ----:| --------------- | ---- |
| Prod  CEPHfs |      1342.07 | 6881 | 1625.13         | 3503 | 1340.45         | 3378 |
| flax         |      1395.27 | 7456 | 2048.63         | 3974 | 1393.22         | 3482 |

File copy from local to CEPHfs kernel
====
ocworker-test Zone: geneva-c


rsync --info=progress2 camera.mov /mnt/opencast_share/camera.mov

  | Bytes          | Percentage | Speed      | Time    |
  | -------------- | ---------- | ---------- | ------- |
  | 7,466,385,408  | 10%        | 248.85MB/s | 0:03:58 |
  | 21,501,149,184 | 31%        | 216.97MB/s | 0:03:30 |
  | 49,788,321,792 | 89%        | 301.41MB/s | 0:00:59 |

| Environment  | Total (Mbps) |  pps | Incoming (kbps) |  pps | Outgoing (Mbps) | pps  |
|:------------ | ------------:| ----:| --------------- | ----:| --------------- | ---- |
| Test  CEPHfs |       4738.49 Mbps  | 98373 |19493.28         | 43018 | 4719.00         | 55354 |
|          |     2554.85| 49062 | 10087.52          | 22353 | 2544.77         | 26708 |




File copy from CEPHfs to Local fuse
====

ocworker-prod-8 Zone: geneva-c

rsync --info=progress2 /mnt/opencast_share/camera3.mov ./.
    851,476,480   1%  139.41MB/s    0:07:51

  | Bytes          | Percentage | Speed      | Time    |
  | -------------- | ---------- | ---------- | ------- |
  | 5,322,604,544  | 7%         | 129.36MB/s | 0:07:54 | 
  | 11,458,871,296 | 16%        | 125.57MB/s | 0:07:21 |
  | 32,841,433,088 | 48%        | 133.70MB/s | 0:04:18 |


| Environment | Total (Mbps) |  pps | Incoming (Mbps) |  pps | Outgoing (kbps) | pps  |
|:----------- | ------------:| ----:| --------------- | ----:| --------------- | ---- |
| Prod CEPHfs |      1039.87 | 6564 | 1038.74         | 4332 | 1124.79         | 2232 |
| flax        |      1074.70 | 6521 | 1073.60         | 4274 | 1105.18         | 2246 | 


ocworker-prod-6 Zone: geneva-a

| Bytes          | Percentage | Speed      | Time    |
| -------------- | ---------- | ---------- | ------- |
| 16,420,732,928 | 24%        | 125.34MB/s | 0:06:43 |
| 24,113,741,824 | 35%        | 138.71MB/s | 0:05:10 |
| 68,207,974,518 | 100%       | 127.59MB/s | 0:08:29 | 



* https://clouddocs.web.cern.ch/file_shares/share_types.html

# Conclusions


* Kernel mount is x5 - x10 faster than fuse (from  65311.39 kbps - 97738.33 kbps during the ingest operation to  598169.62 kbps)
* Ingest track improved. The time required goes from 1h50m-2h30m to 12m
* The "heavy" inspect operation is improved. The time required goes from 45m-60m to 17m
* kernel file copy is 2-3 times faster than fuse.
* Bug detected in Centos Stream 8 when using CEPH kernel mount:
Notified to the Storage Group. Debug and reported by Dan van der Ster
[https://its.cern.ch/jira/browse/CRM-4180] cephfs: disable async dirops on stream 8 kernel 358 - CERN Central Jira
Bug #54013:https://tracker.ceph.com/issues/54013 centos stream 8 kernel 358: async dirops causes Cannot write: Operation not permitted - Linux kernel client - Ceph

# Actions



Set sestatus to permissive

```bash
getenforce
Permissive
```

```puppet
  class { selinux:
    mode => 'permissive',
    type => 'targeted',
  }
```



```bash
[root@ocworker-test-2 ~]# sestatus
SELinux status:                 enabled
SELinuxfs mount:                /sys/fs/selinux
SELinux root directory:         /etc/selinux
Loaded policy name:             targeted
Current mode:                   permissive
Mode from config file:          permissive
Policy MLS status:              enabled
Policy deny_unknown status:     allowed
Memory protection checking:     actual (secure)
Max kernel policy version:      33
```

Select the appropriate ceph type

```puppet

    file {'/mnt/opencast_share':
      ensure => directory,
      owner  => 'opencast',
      group  => 'opencast',
      mode   => '0775'    
    }    
    -> cephfs {'/mnt/opencast_share':
      cluster     => $ceph_opencast_cluster,
      remote_path => $ceph_opencast_remote,
      id          => $ceph_opencast_id,
      type        => 'kernel'
      #mount_options => 'wsync,rw'
    }
```

The mount options are by applied by default since the Bug was notified

Ensure that the -purge is included in yaml declaration:
frontend.yaml
worker.yaml
ingest.yaml

```yaml

pluginsync_filter_enable: True
pluginsync_filter:
  - purge
```



Unmount the share before puppet update

```bash

systemctl stop opencast
umount -f -l /mnt/opencast_share
umount -f -l /mnt/media_share
puppet agent -tv
```

For the worker also:

```bash
umount -f -l /mnt/master_share
```

A host reboot is recommended.


