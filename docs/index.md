Transcoding as a Service (TaaS) Manifest
==

The transcoding (also known as transrate) service tries to cover the need of certain CERN apps and services regarding video conversions also known as sub-formats. The service is based on a FOSS project: [Opencast](https://opencast.org/) which is heavily based on [ffmpeg](https://www.ffmpeg.org/) to do the conversions. 

The service API in order to tranfer/query jobs/download results is following Opencast guidelines and best practices. The service interface to external CERN apps/services should be the same for all of them. This implies an agnostic view as internal workflows should be unaware of how those user application/services internally work.
Any request that differs from mainstream are simply not maintanable and therefore not possible. The service will try to keep up-to-date with upstream versions.

The service clients should ask for the following resources in order to access the TaaS [External API](https://docs.opencast.org/develop/developer/#api/) or WebUI:
* At least one Opencast Serie to link the ingested/uploaded videos and metadata files, and whose ACLs will be applied.
* At least one Opencast local USER or ROLE to map to one or more CERN eGroups or Service accounts to get the required permissons for using the API.



The client is reponsible for the upload of his/her source files and download of the encoded resulting files. 
Those files will be available in the server for downloading during a limited period of time.



The service is running on CERN openstack virtual machines. And it's limited on the resources that such virtualization is offering. As resources are shared, no priorities will be stablished for specific tasks. However the global resources could be increased as needed. 

Nowadays running on ... we have observed following limits:
[Performance](./performance/netrates.md)